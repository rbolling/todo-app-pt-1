import React, { Component } from "react";
import todosList from "./todos.json";

import TodoList from "./components/todo-list/TodoList";

class App extends Component {
  state = {
    todos: todosList,
    toDoInput: ''
  }

  handleChange = (event) => {
    this.setState({ toDoInput: event.target.toDoInput});
  }

  handleSubmit = (event) => {
    const newTodos = this.state.todos;
    event.preventDefault();
    const newTodo = {
      userId : 1,
      id : Math.random(),
      title : document.getElementById("testing").value,
      completed : false
    }
    newTodos.push(newTodo);
    this.setState({ newTodo });
   
    document.getElementById("testing").value = "";
  }

  

  handleDelete = todoTitle => event => {
    const newTodos = this.state.todos.filter(
      todo => todo.title !== todoTitle
    )
    this.setState({ todos: newTodos })
  }

  handleDeleteAll = todoTitle => event => {
    const newTodos = this.state.todos.filter(function(todo){return todo.completed === false})
    this.setState({ todos: newTodos })
  }

  handleCompleted = todoTitle => event => {
    const newTodos = this.state.todos.filter(function(todo) {
      if (todo.title === todoTitle) {
        todo.completed = !todo.completed
      }
    });
    
    this.setState({ newTodos })
  }
  


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
            <input 
            className="new-todo" 
            id="testing"
            placeholder="What needs to be done?" 
            autoFocus 
            value={this.state.toDoInput} 
            onChange={this.handleChange}
            />
          </form>
        </header>
        <TodoList 
          todos={this.state.todos} 
          del={this.handleDelete}
          changeComplete={this.handleCompleted}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteAll()}>Clear completed</button>
        </footer>
      </section>
    );
  }
}


export default App;

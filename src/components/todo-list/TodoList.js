import React, { Component } from 'react';

import TodoItem from '../todo-item/TodoItem'

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem 
                title={todo.title} 
                completed={todo.completed} 
                key={todo.id} del={this.props.del} 
                changeComplete={this.props.changeComplete} 
              />
            ))}
          </ul>
        </section>
      );
    }
  }

export default TodoList;